﻿(function (t) {
    "function" == typeof define && define.amd ? define(["jquery", "moment"], t) : t(jQuery, moment);
})(function (t, e) {
    (e.defineLocale || e.lang).call(e, "he", {
        months: "ינואר_פברואר_מרץ_אפריל_מאי_יוני_יולי_אוגוסט_ספטמבר_אוקטובר_נובמבר_דצמבר".split("_"),
        monthsShort: "ינואר_פברואר_מרץ_אפריל_מאי_יוני_יולי_אוגוסט_ספטמבר_אוקטובר_נובמבר_דצמבר".split("_"),
        weekdays: "ראשון_שני_שלישי_רביעי_חמישי_שישי_שבת".split("_"),
        weekdaysShort: "'א_'ב_'ג_'ד_'ה_'ו_שבת".split("_"),
        weekdaysMin: "'א_'ב_'ג_'ד_'ה_'ו_שבת".split("_"),
        longDateFormat: {
            LT: "HH:mm",
            L: "DD/MM/YYYY",
            LL: "D MMMM YYYY",
            LLL: "D MMMM YYYY LT",
            LLLL: "dddd, D MMMM YYYY LT"
        },
        calendar: {
            sameDay: "[Today at] LT",
            nextDay: "[Tomorrow at] LT",
            nextWeek: "dddd [at] LT",
            lastDay: "[Yesterday at] LT",
            lastWeek: "[Last] dddd [at] LT",
            sameElse: "L"
        },
        relativeTime: {
            future: "בעוד %s",
            past: "לפני %s",
            s: "a few seconds",
            m: "דקה",
            mm: "%d דקות",
            h: "שעה",
            hh: "%d שעות",
            d: "יום",
            dd: "%d ימים",
            M: "חודש",
            MM: "%d חודשים",
            y: "שנה",
            yy: "%d שנים"
        },
        week: {
            dow: 1,
            doy: 4
        }
    }), t.fullCalendar.datepickerLang("he", "Hebrew", {
        closeText: "סיום",
        prevText: "הקודם",
        nextText: "הבא",
        currentText: "היום",
        monthNames: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
        monthNamesShort: ['ינו', 'פבר', 'מרץ', 'אפר', 'מאי', 'יוני', 'יולי', 'אוג', 'ספט', 'אוק', 'נוב', 'דצמ'],
        dayNames: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת"],
        dayNamesShort: ['\'א', '\'ב', '\'ג', '\'ד', '\'ה', '\'ו', 'שבת'],
        dayNamesMin: ['\'א', '\'ב', '\'ג', '\'ד', '\'ה', '\'ו', 'שבת'],
        weekHeader: "שבוע",
        dateFormat: "dd/mm/yy",
        firstDay: 0,
        isRTL: 1,
        showMonthAfterYear: 0,
        yearSuffix: ""
    }), t.fullCalendar.lang("he", {
        defaultButtonText: {
            month: "חודש",
            week: "שבוע",
            day: "יום",
            list: "סדר יום"
        },
        allDayText: "כל היום",
        eventLimitText: "אחר"
    }), t.fullCalendar.lang("he", {
        titleFormat: {
            month: 'MMMM YYYY',
            week: "LL",
            day: 'dddd, MMM D, YYYY'
        },
        columnFormat: {
            month: 'ddd',
            week: 'D/M ddd',
            day: 'dddd D/M'
        }
    });
});