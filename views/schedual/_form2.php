<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use kartik\color\ColorInput;
use kartik\range\RangeInput;
use kartik\file\FileInput;
use kartik\switchinput\SwitchInput;
use kartik\sidenav\SideNav;





/* @var $this yii\web\View */
/* @var $model app\models\Schedual */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedual-form">
<?php
echo SideNav::widget([
    'type' => SideNav::TYPE_DEFAULT,
    'heading' => 'Options',
    'items' => [
        [
            'url' => '#',
            'label' => 'Home',
            'icon' => 'home'
        ],
        [
            'label' => 'Help',
            'icon' => 'question-sign',
            'items' => [
                ['label' => 'About', 'icon'=>'info-sign', 'url'=>'#'],
                ['label' => 'Contact', 'icon'=>'phone', 'url'=>'#'],
            ],
        ],
    ],
]);
?>
    <?php $form = ActiveForm::begin(); ?>

	<?php
// Usage with ActiveForm and model
echo $form->field($model, 'employeesName')->widget(ColorInput::classname(), [
    'options' => ['placeholder' => 'Select color ...'],
]);

// With model & without ActiveForm
echo '<label class="control-label">Select Color</label>';
echo ColorInput::widget([
    'model' => $model,
    'attribute' => 'employeesName',
]);
?>

<?php
echo $form->field($model, 'employeesName')->widget(RangeInput::classname(), [
    'options' => ['placeholder' => 'Select range ...'],
    'html5Options' => ['min'=>0, 'max'=>1, 'step'=>1],
    'addon' => ['append'=>['content'=>'star']]
]);

// With model & without ActiveForm
echo '<label class="control-label">Adjust Contrast</label>';
echo RangeInput::widget([
    'model' => $model,
    'attribute' => 'employeesName',
    'addon' => ['append'=>['content'=>'%']]
]);
    ?>
			<?= $form->field($model, 'employeesName')->textInput(['readonly' => true,'value' =>Yii::$app->user->identity->username]) ?>
		
	<?= 
  $form->field($model, 'month')            
         ->dropDownList(['ינואר','פברואר','מרץ','אפריל','מאי','יוני','יולי','אוגוסט','ספטמבר','אוקטובר','נובמבר','דצמבר'],['prompt' => 'בחר חודש'])->label("בחר חודש");
 ?>
<?php
/*echo $form->field($model, 'employeesName')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*'],
]);*/

// With model & without ActiveForm
echo '<label class="control-label">Add Attachments</label>';
echo FileInput::widget([
    'model' => $model,
    'attribute' => 'employeesName',
    'options' => ['multiple' => true]
]);
?>
<?php // Usage with ActiveForm and model
echo $form->field($model, 'employeesName')->widget(SwitchInput::classname(), [
    'type' => SwitchInput::CHECKBOX
]);


// With model & without ActiveForm
echo SwitchInput::widget([
    'name' => 'employeesName',
    'type' => SwitchInput::CHECKBOX
]);

?>
<head>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  
  <script>
  $(document).ready(function() {
    $("#datepicker").datepicker();
  });
  </script>
</head>
<body>
<form>
    <input id="datepicker" multiple/>
</form>
</body>

    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>

  <body>
  
    <h3>חישוב נסיעות לעובדים</h3>
    <div id="map"></div>
    <script>
      function initMap() {
        var uluru = {lat: 31.831702 , lng: 35.306956};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHfNO9bw-2csb7EBdtQhe9SGjQ9N-CHgw&callback=initMap">
    </script>
	<!--<iframe src="http://www.govmap.gov.il/" height="200" width="300"></iframe>-->
 

</body>
</html>
	<?= $form->field($model, 'days')->widget(
    DatePicker::className(), [
        // inline too, not bad
		'inline' => true, 
		'language' => 'he',
		//'class'=> 'well well-sm',
		//'style'=>'background-color: #fff; width:250px',
		
        // modify template for custom rendering
        'template' => '<div class="fa fa-long-arrow-right">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
			 'multidate' => true,
			        'format' => 'dd/mm/yyyy',
					'clearBtn' => true,
					
        ]
]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'צור' : 'עדכן', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
