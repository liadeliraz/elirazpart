<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ImageManager */

$this->title = 'Create Image Manager';
$this->params['breadcrumbs'][] = ['label' => 'Image Managers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-manager-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
