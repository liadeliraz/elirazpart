<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Employees;
use kartik\select2\Select2;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>

	
	<?php $month = $model->month;?>
	 <?php $day = $model->created_date;?>
	 <?php $dt = new DateTime("$day");?>
	 <?php
	 $workDays = \app\models\Schedual::Cal_Days_per_month($month,$dt);
	  "בחודש זה ישנם $workDays ימי עבודה רגילים ללא סופי שבוע.";
		?>
		<?php
		$teken = round($workDays*0.7);
		$houersTeken70 = round($workDays*8.5*0.7);
		$teken100 = round($workDays*1);
		$houersTeken100 = round($workDays*8.5*1);
		
		$year1 = $dt->format('Y');
			$month1 = $dt->format('m');
		
		 $teken40 = round(cal_days_in_month(CAL_GREGORIAN, $month1,$year1)*0.4);
		 $houersTeken40 = round(cal_days_in_month(CAL_GREGORIAN, $month1,$year1)*8.5*0.4);
		
		
		
		
			?>
		<details >
		<summary class = "btn btn-info" >נתוני שיבוץ נוספים</summary>
<?php
			echo '<h4 style = "text-decoration: underline; color:#696969"> 100% משרה:</h4>';
			echo '<r style = "color:#2F4F4F">עובדים עם תקן 100% צריכים להיות משובצים '."$teken100".' משמרות, סך הכל השעות שיצטרכו הינם '."$houersTeken100".'</r>';
			echo ' <br>';
			$percent100Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>3])->all();
			$percent100Array = ArrayHelper::getColumn($percent100Sort, 'id');	
			for($k100 = 0; $k100 < sizeof($percent100Array);$k100++)
			{
			$get100arr = \app\models\Events::find()->where(['employees'=>$percent100Array[$k100],'month'=>$model->month])->all();
			$a100Array = ArrayHelper::getColumn($get100arr, 'id');
			$countDays100 = sizeof($a100Array);
			if(sizeof($a100Array)>0) {
			$nameOfEmployee100 = \app\models\Employees::findOne($percent100Array[$k100])->fullname;
			$houersEmployee100 = $countDays100*8.5;
			
			$Weekend100Array = ArrayHelper::getColumn($get100arr, 'created_date');
			////// calculate weekends////////////////
			for($FRI100 = 0; $FRI100 < sizeof($Weekend100Array);$FRI100++){
			$myTimew100 = strtotime($Weekend100Array[$FRI100]);
			$dayW100 = date("D",$myTimew100 ); // Sun - Sat
			if($dayW100 == "Fri")
			$houersEmployee100 = $houersEmployee100 - 3.5;
			}
			
			for($SAT100 = 0; $SAT100 < sizeof($Weekend100Array);$SAT100++){
			$myTimew100 = strtotime($Weekend100Array[$SAT100]);
			$dayW100 = date("D",$myTimew100 ); // Sun - Sat
			if($dayW100 == "Sat")
			$houersEmployee100 = $houersEmployee100 - 8.5;
			}
			
			echo " $nameOfEmployee100 שובץ $countDays100 משמרות, סך הכל בשעות $houersEmployee100 ";
			
			if($houersEmployee100 < $houersTeken100){
			$hoserSumHouers100 = round($houersTeken100 - $houersEmployee100);
			$hoserSumDays100 = round($hoserSumHouers100/8.5);
			if($hoserSumDays100!=0){
			echo '<p12 style = "color:red; font-size:bold"> - חסר ל'.$nameOfEmployee100.' '.$hoserSumHouers100.' שעות או- '.$hoserSumDays100.' ימים</p12>';
			}else{
			echo '<p13 style = "color:green; font-size:bold"> - חסר ל'.$nameOfEmployee100.' '.$hoserSumHouers100.' שעות  - שיבוץ תקין</p13>';
			}
			}
			echo '<br>';
			}
			
			}
				echo ' <hr>';
		
			echo '<h4 style = "text-decoration: underline; color:#696969"> 70% משרה:</h4>';
			echo '<r style = "color:#2F4F4F">עובדים עם תקן 70% צריכים להיות משובצים '."$teken".' משמרות, סך הכל השעות שיצטרכו הינם '."$houersTeken70".'.</r>';
			echo ' <br>';
			$percent70Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>2])->all();
			$percent70Array = ArrayHelper::getColumn($percent70Sort, 'id');	
			for($k70 = 0; $k70 < sizeof($percent70Array);$k70++)
			{
			$get70arr = \app\models\Events::find()->where(['employees'=>$percent70Array[$k70],'month'=>$model->month])->all();
			$a70Array = ArrayHelper::getColumn($get70arr, 'id');
			$countDays = sizeof($a70Array);
			if(sizeof($a70Array)>0) {
			$nameOfEmployee = \app\models\Employees::findOne($percent70Array[$k70])->fullname;
			$houersEmployee = $countDays*8.5;
			
			$Weekend70Array = ArrayHelper::getColumn($get70arr, 'created_date');
			////// calculate weekends////////////////
			for($FRI70 = 0; $FRI70 < sizeof($Weekend70Array);$FRI70++){
			$myTimew70 = strtotime($Weekend70Array[$FRI70]);
			$dayW70 = date("D",$myTimew70 ); // Sun - Sat
			if($dayW70 == "Fri")
			$houersEmployee = $houersEmployee - 3.5;
			}
			
			for($SAT70 = 0; $SAT70 < sizeof($Weekend70Array);$SAT70++){
			$myTimew70 = strtotime($Weekend70Array[$SAT70]);
			$dayW70 = date("D",$myTimew70 ); // Sun - Sat
			if($dayW70 == "Sat")
			$houersEmployee = $houersEmployee - 8.5;
			}
			
			echo " $nameOfEmployee שובץ $countDays משמרות, סך הכל בשעות $houersEmployee ";
			if($houersEmployee < $houersTeken70){
			$hoserSumHouers = round($houersTeken70 - $houersEmployee);
			$hoserSumDays = round($hoserSumHouers/8.5);
			if($hoserSumDays!=0){
			echo '<p12 style = "color:red; font-size:bold"> - חסר ל'.$nameOfEmployee.' '.$hoserSumHouers.' שעות או- '.$hoserSumDays.' ימים</p12>';
			}else{
			echo '<p13 style = "color:green; font-size:bold"> - חסר ל'.$nameOfEmployee.' '.$hoserSumHouers.' שעות  - שיבוץ תקין</p13>';
			}
			}
			echo '<br>';
			}
			
			}
			echo ' <hr>';
			
			echo '<h4 style = "text-decoration: underline; color:#696969"> 40% משרה:</h4>';
			echo '<r style = "color:#2F4F4F">עובדים עם תקן 40% צריכים להיות משובצים '."$teken40".' משמרות, סך הכל השעות שיצטרכו הינם '."$houersTeken40".'.</r>';
			echo ' <br>';
			$percent40Sort = \app\models\Employees::find()->where(['Percent_of_jobs'=>0])->all();
			$percent40Array = ArrayHelper::getColumn($percent40Sort, 'id');	
			for($k40 = 0; $k40 < sizeof($percent40Array);$k40++)
			{
			$get40arr = \app\models\Events::find()->where(['employees'=>$percent40Array[$k40],'month'=>$model->month])->all();
			$a40Array = ArrayHelper::getColumn($get40arr, 'id');
			$countDays40 = sizeof($a40Array);
			if(sizeof($a40Array)>0) {
			$nameOfEmployee40 = \app\models\Employees::findOne($percent40Array[$k40])->fullname;
			$houersEmployee40 = $countDays40*8.5;
			
			$temp40 = 0 ; 
			echo " $nameOfEmployee40 שובץ $countDays40 משמרות, סך הכל בשעות $houersEmployee40 ";
			if($houersEmployee40 > $houersTeken40){
			echo '<p12 style = "color:red; font-size:bold">העובד '.$nameOfEmployee40.' חרג החודש מ- 40% משרה</p12>';
			$temp40 =1; 
			}
			if($temp40 == 0 )
			echo '<p14 style = "color:green; font-size:bold">שיבוץ תקין</p14>';
			echo '<br>';
			}
			
			}
			echo ' <hr>';
	 
	 ?>
			</details>
			
	 <?php
				echo ' <hr>';
		?>
	 
	<?=  $form->field($model, 'month')->hiddenInput()->label(false)?>
	 <?= $dateF = $form->field($model, 'created_date')->hiddenInput(['format' => 'dd/mm/yyyy', 'readonly'=>true])->label(false) ?>
 <div style = "text-align:right">
 <?=  $form->field($model, 'team_leader')->dropDownList(\app\models\Schedual::getDays($month,$day,$dt),['placeholder' => 'ראש צוות','required'=> true,'maxlength' => true]) ?>
 
 <?=  $form->field($model, 'cashier')->widget(Select2::classname(), [
   
    'data' => \app\models\Schedual::getDays($month,$day,$dt),
	//'data' => Employees::getEmployees(),

    'language' => 'he',
	
	'options' => ['required'=> true,'dir' => 'rtl','multiple' => true],
    'pluginOptions' => [
	'allowClear' => true,
	'placeholder' => ' בחר עובדים',
    'tags' => true,
    ],
]);?>
 <?=  $form->field($model, 'employees')->widget(Select2::classname(), [
   
    'data' => \app\models\Schedual::getDays($month,$day,$dt),
	//'data' => Employees::getEmployees(),

    'language' => 'he',
	
	'options' => ['required'=> true,'dir' => 'rtl','multiple' => true],
    'pluginOptions' => [
	'allowClear' => true,
	'placeholder' => ' בחר עובדים',
    'tags' => true,
    ],
]);
?>



 </div>
	
	
	 <?=  $form->field($model, 'holyday')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'projects')->textInput(['maxlength' => true]) ?>

   
	


    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
	
	
	<?php $this->registerJsFile('app/web/main2.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'צור' : 'עדכן', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
