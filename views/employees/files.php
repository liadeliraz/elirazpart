
<?php 
use kartik\widgets\FileInput;
 use yii\helpers\Url;


echo '<label class="control-label">הוסף מסמכים</label>';
echo FileInput::widget([
    'model' => $model,
    'name' => 'ImageManager[attachment]',
    'options' => ['multiple' => true],
	 'pluginOptions' => [
		'uploadUrl' => \yii\helpers\Url::to(['/employees/upload-files']),
		'browseIcon' => '<i style = "margin-left:5%;" class="glyphicon glyphicon-paperclip"></i> ',
        'browseLabel' =>  'בחר קבצים',
		'removeLabel' =>  'מחק קבצים',
		'uploadLabel' => 'העלה קבצים',
		'uploadExtraData' => [
            'ImageManager[item_id]' => $model->id  
        ],
        'maxFileCount' => 15
	],
]);

//echo "$model->fullname"
?>

