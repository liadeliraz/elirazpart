<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
 
<h3><?= $msg ?></h3>
 
<h1>איפוס סיסמה</h1>
<?php $form = ActiveForm::begin([
    'method' => 'post',	
	//'id' => 'test1',
	 //'action' => Url::toRoute(\Yii::$app->request->getPathInfo())
    'enableClientValidation' => true,
]);
?>

<div class="form-group">
 <?= $form->field($model, "email")->input("email") ?>  
</div>
 
<div class="form-group">
 <?= $form->field($model, "password")->input("password") ?>  
</div>
 
<div class="form-group">
 <?= $form->field($model, "password_repeat")->input("password") ?>  
</div>

<div class="form-group">
 <?= $form->field($model, "verification_code")->input("text") ?>  
</div>

<div class="form-group">
 <?= $form->field($model, "recover")->input("hidden")->label(false) ?>  
</div>
 
<?= Html::submitButton("שנה סיסמה", ["class" => "btn btn-primary"]) ?>
 
   <?php ActiveForm::end(); ?>