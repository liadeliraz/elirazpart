<?php

namespace app\controllers;

use Yii;
use app\models\Events;
use app\models\Employees;
use app\models\EventsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\bootstrap\Modal;
use yii\widget\Pjax;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControle;




/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller

{
    public function behaviors()
    {
         return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Event models.
     * @return mixed
     */
	 
	


    public function actionIndex()
    {
		
        $events = Events::find()->all();
		$tasks = [];
		//////////////////////////////////ראש צוות///////////////////////////////
		/*$allEvents = Events::find()->select('created_date')->distinct()->asArray()->all();
		$resul33 = ArrayHelper::getColumn($allEvents, 'created_date');
		
		 $CreatedCulome  = Events::find()->select('created_date')->distinct()->all();
		 $CreatedArray = ArrayHelper::toArray($CreatedCulome);
		 $counter1 = 0;
		for($TL = 0; $TL < sizeof($CreatedCulome);$TL++){
	  
		$TeanLeader = Events::find()->select('team_leader')->where(['created_date' => $CreatedCulome[$TL]])->one();
		
		$event = new \yii2fullcalendar\models\Event();
		$color = \app\models\Employees::findOne($TeanLeader)->color;
		$event->backgroundColor = $color;
		$event->className = 'btn';
		$name = \app\models\Employees::findOne($TeanLeader)->fullname;
		$role = \app\models\Employees::findOne($TeanLeader)->roleItem->name;
		$event->title = "$name - ראש צוות";
		$event->start = $resul33[$TL];
		$tasks[] = $event;
		 $counter1++;
		
		}*/
		
		
		
		////////////////////////////////////////////////////////////////////
		
		
		/*//////////////////////////////////קופאים///////////////////////////////
		$allEvents = Events::find()->select('created_date')->distinct()->asArray()->all();
		$resul33 = ArrayHelper::getColumn($allEvents, 'created_date');
		
		 $CreatedCulome  = Events::find()->select('created_date')->distinct()->all();
		 $CreatedArray = ArrayHelper::toArray($CreatedCulome);
		 $counter1 = 0;
		for($TL = 0; $TL < sizeof($CreatedCulome);$TL++){
	  
		$TeanLeader = Events::find()->select('team_leader')->where(['created_date' => $CreatedCulome[$TL]])->one();
		
		$event = new \yii2fullcalendar\models\Event();
		$color = \app\models\Employees::findOne($TeanLeader)->color;
		$event->backgroundColor = $color;
		$event->className = 'btn';
		$name = \app\models\Employees::findOne($TeanLeader)->fullname;
		$role = \app\models\Employees::findOne($TeanLeader)->roleItem->name;
		$event->title = "$name - ראש צוות";
		$event->start = $resul33[$TL];
		$tasks[] = $event;
		 $counter1++;
		
		}
		
		
		
		////////////////////////////////////////////////////////////////////*/
		
		
		
		
        foreach ($events as $eve) 
        {
		 
          $event = new \yii2fullcalendar\models\Event();
		  $color = \app\models\Employees::findOne($eve->employees)->color;
          $event->backgroundColor = $color;
		  $event->className = 'btn';
		  $event->id = "$eve->created_date,$eve->employees";
		  //$eve->employees=explode(', ',$eve->employees);//converting to array...
		  $counter = count($eve->employees);
         // $event->title = "$eve->employees  $eve->created_date";
		   //$event->title = $eve->employees[0];
			
		  $name = \app\models\Employees::findOne($eve->employees)->fullname;
		  $role = \app\models\Employees::findOne($eve->employees)->roleItem->name;
		  $event->title = "$name";
		  if("$eve->employees" == "$eve->team_leader"){
		  $event->title = "$name - ראש צוות";
		  }
		  $resultCashier = explode(',',$eve->cashier );
		  for ($c = 0; $c <sizeof($resultCashier) ; $c++){
		  if("$eve->employees" == "$resultCashier[$c]")
		   $event->title = "$name - קופאי";
		  }
		  $event->start = $eve->created_date;
          $tasks[] = $event;
		  }
		  
		   return $this->render('index', [
		
            'events' => $tasks,
        ]);
    }
	

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	/////////////////////////////////// SIMPLE CREAT FUNCTION FROM PROJECTS ///////////////
	/* public function actionCreate()
    {
			$model = new Projects();
                if(isset($_POST['Projects']))
                {
                        $model->attributes=$_POST['Projects'];
                        if($model->employee!=='')
                                $model->employee=implode(', ',$model->employee);//converting to string...
                        if($model->save())
                                $this->redirect(array('view','id'=>$model->id));
                }
				   else {
            return $this->render('create', [
                'model' => $model,
            ]);
               $model->employee=explode(', ',$model->employee);//converting to array...
                $this->render('create',array(
                        'model'=>$model,
                ));
        }
		}
	
///////////////////////////////////////////////////////////////////////////////////////////*/	
	
	
	
    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($date,$monthName)
    {	
		
		
		$result = 1;
		for ($i = 0; $i <= $result; $i++) {
		$model = new Events();
		$model->created_date = $date;
		$model->month = $monthName;
		
		if(isset($_POST['Events']))
		 {
						
                        $model->attributes=$_POST['Events'];
						$result = count($model->employees);
						$result = $result - 1;
                        if($model->employees!=='')
                                $model->employees=$model->employees[$i];//implode(', ',$model->employees);//converting to string...
								$model->cashier=implode(', ',$model->cashier);
								$model->id = "$date,$model->employees";
                        if($model->save())
							$this->redirect(['index']);
                }
        
         else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }
	}
	
	///////////////BECKUP//////////////////////////////////////*
	
	/*public function actionCreate($date)
    {
        
		
		for ($i = 1; $i <= 2; $i++) {
		$model = new Events();
		 if(isset($_POST['Events']))
		 {
						$model->created_date = $date;
                        $model->attributes=$_POST['Events'];
                        if($model->employees!=='')
                                $model->employees=implode(', ',$model->employees);//converting to string...
                        if($model->save())
							$this->redirect(['index']);
                }
        
		//$model->id = $date;

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);*/
        /* else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }
	}*/

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
