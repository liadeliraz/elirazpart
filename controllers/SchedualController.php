<?php

namespace app\controllers;

use Yii;
use app\models\Schedual;
use yii\data\SqlDataProvider;
use app\models\SchedualSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControle;

/**
 * SchedualController implements the CRUD actions for Schedual model.
 */
class SchedualController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
      
		
		 return [
			'access'=>[
			     'class'=>\yii\filters\AccessControl::className(),
				 'only'=>['create','update','index','view','delete','export','chart2'],
				 'rules'=>[
				    [
						'allow'=>true,
						'roles'=>['@']
					],			 
				 ]	
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Schedual models.
     * @return mixed
     */
    public function actionIndex()
    {
	 if (\Yii::$app->user->can('createUser')) { 
		$searchModel = new SchedualSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		} 
		
		
		 if (!\Yii::$app->user->can('createUser')){
		$UserName = Yii::$app->user->identity->username;
        $searchModel = new SchedualSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		//$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		//'.$UserName.'
		$UserName = Yii::$app->user->identity->username;
		$dataProvider = new SqlDataProvider([
      'sql' => 'SELECT * FROM `schedual` WHERE `employeesName` = "'.$UserName.'" ORDER BY `schedual`.`month` ASC',
     // 'totalCount' => $count,
      
      'sort' => [
         'attributes' => [
            'employeesName',
            'days',
			
           
         ],
      ],
   ]);
		}
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Schedual model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Schedual model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schedual();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
		/////////////////////////////////////////////////////////////
		
		
		/*$model = new Schedual();
                if(isset($_POST['Schedual']))
                {
                        $model->attributes=$_POST['Schedual'];
                        if($model->days!=='')
                                $model->days=implode(', ',$model->days);//converting to string...
                        if($model->save())
                                $this->redirect(array('view','id'=>$model->id));
                }
				   else {
            return $this->render('create', [
                'model' => $model,
            ]);
               $model->days=explode(', ',$model->days);//converting to array...
                $this->render('create',array(
                        'model'=>$model,
                ));
        }*/
		
    }

    /**
     * Updates an existing Schedual model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->month]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Schedual model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schedual model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Schedual the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schedual::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
