<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms".
 *
 * @property integer $id
 * @property string $phonenum
 * @property string $smssubject
 * @property string $smsmes
 */
class Sms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phonenum', 'smssubject', 'smsmes'], 'required'],
            [['phonenum', 'smssubject', 'smsmes'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phonenum' => 'מספר טלפון',
            'smssubject' => 'נושא',
            'smsmes' => 'הודעה',
        ];
    }
}
