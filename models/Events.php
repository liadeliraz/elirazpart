<?php

namespace app\models;

use Yii;
use yii\data\SqlDataProvider;
use yii\filters\VerbFilter;
/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $employees
 * @property string $projects
 * @property string $holyday
 * @property string $description
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employees'], 'required'],
            [[ 'projects', 'holyday', 'description','month','team_leader','cashier'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employees' => 'עובדים',
            'projects' => 'פרויקטים',
            'holyday' => 'חגים',
            'description' => 'הערות',
			 'month' => 'חודש',
			'team_leader' => 'ראש צוות',
			'created_date' => 'שיבוץ ליום',
			'cashier'=> 'צוות קופה',
        ];
    }
	/*public static function getMonth($monthName)
	{
	//$month = $monthName;
	return $monthName;
	}*/
	
	public static function getDays()
	{
		
		$monn = 'מרץ 2017';
		$allDays = \app\models\Schedual::find('employeeId')->where(['month' => $monn])->all();
		$result = ArrayHelper::getColumn($allDays, 'employeeId');
		
		return $result;		

	}
	/*public static function getEmployeesDays()
	{
		/*$dataProvider = new SqlDataProvider([
        'sql' => 'SELECT * FROM `schedual` WHERE `employeesName` = "'.$UserName.'" ORDER BY `schedual`.`month` ASC';
		
		
		
		$allEmployees = self::find()->all();
		$allEmployeesArray = ArrayHelper::
					map($allEmployees, 'id', 'fullname');
		return $allEmployeesArray;						
	}*/
}
